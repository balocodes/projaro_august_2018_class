// Express is a library that makes it easy to 
// create servers in nodejs
const express = require("express");
// body-parser converts posted form data to 
// data that can easily be manipulated
const bodyParser = require("body-parser");
// userRouter and miscRouter contain other routes
// that have been moved from server.js for neater
// and more maintainable code
const userRouter = require("./routes/users-routes");
const miscRouter = require("./routes/misc-routes");
const googleRouter = require("./routes/google-routes");

// initialize express
const app = express();

// use body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// serve static files from the described locations
app.use(express.static(__dirname + "/assets/css"))
app.use(express.static(__dirname + "/html"))
app.use(express.static(__dirname + "/google"))

// use other routers
app.use("/user", userRouter);
app.use("/misc", miscRouter);
app.use("/google", googleRouter);

app.get("/", function(req, res) {
    res.send("Server works!");
});

// serve an HTML page to the user's browser
app.get("/registration-page", (req, res) => {
    res.sendFile(__dirname +"/html/index.html");
})



// If request does not match any of the above routes
app.get("*", (req, res) => {
    res.sendFile(__dirname + "/html/404.html");
})

// create and start listening to the server on port 2800
app.listen(2800, console.log("Server running on port 2800"));