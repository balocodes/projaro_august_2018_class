const express = require("express");
// create an instance of a router
const router = express.Router();

router.post("/register", (req, res) => {
    console.log(req.body);
    res.send("Thanks for registering " + req.body.fname + "!");
})

router.get("/welcome/:name/:nickname", (req, res) => {
    console.log(req.params);
    res.send("Welcome to Projaro Mr. " + req.params.name + ". Your nickname is " + req.params.nickname);
})

// export the router so that it may be used in other places
module.exports = router;