const express = require("express");
const router = express.Router();

router.get("/test", (req, res) => {
    res.send("Test route works");
});

router.get("/water", (req, res) => {
    console.log(req.query);
    res.send("Query works")
})

module.exports = router;